from jinja2 import Environment, FileSystemLoader, select_autoescape

import importlib
import os
import shutil

APPS = ['frlsim']
OUT_DIR = 'public'

class Core:
	def __init__(self):
		self.env = Environment(
			loader=FileSystemLoader([app + '/jinja2' for app in APPS]),
			autoescape=select_autoescape(['html', 'xml'])
		)
	
	def render(self, output, template, **kwargs):
		print('Rendering {}'.format(output))
		
		path = os.path.join(OUT_DIR, output)
		if path.endswith('/'):
			path += 'index.html'
		os.makedirs(os.path.dirname(path), exist_ok=True)
		
		with open(path, 'w') as f:
			print(self.env.get_template(template).render(**kwargs), file=f)
	
	def redirect(self, output, destination):
		self.render(output, 'redirect.html', destination=destination)
	
	def copy(self, output, source):
		print('Copying {}'.format(output))
		
		path = os.path.join(OUT_DIR, output)
		if path.endswith('/'):
			path += 'index.html'
		os.makedirs(os.path.dirname(path), exist_ok=True)
		
		shutil.copyfile(source, path)
	
	def write(self, output, content):
		print('Writing {}'.format(output))
		
		path = os.path.join(OUT_DIR, output)
		if path.endswith('/'):
			path += 'index.html'
		os.makedirs(os.path.dirname(path), exist_ok=True)
		
		with open(path, 'w') as f:
			print(content, file=f)

core = Core()

for app in APPS:
	print('Generating {}'.format(app))
	importlib.import_module(app + '.generate').generate(core)

---
title: Australian Securities and Investments Commission (Authorised Deposit-taking Institutions) Direction 2019
maker: FinePorpoise
made: 2019-05-26
urls:
- https://docs.google.com/document/d/1JU0nSvDpc8-bjjhvBeQEXyKtJLOrzdG5NjE3_IqfpbU/edit
- https://www.reddit.com/r/AustraliaSimLower/comments/bthiv6/australian_securities_and_investments_commission/
- https://www.reddit.com/r/AustraliaSimUpper/comments/bwp84j/1214c_motion_for_disallowance_australian/
made_under: Australian Securities and Investments Commission Act 2001
disallowed: 2019-06-05
---

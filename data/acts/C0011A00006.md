---
title: Oranga Tamariki (Youth Court Age) Amendment Act 2020
bill: 271
introducer: frod02000
author:
- forgottomentionpeter (Greens)
party: Greens
portfolio: Justice
assent: 2020-05-24
commencement: 2020-05-25
first_reading: reddit.com/r/ModelNZParliament/comments/gc1mkq/b271_oranga_tamariki_youth_court_age_amendment/
committee: reddit.com/r/ModelNZParliament/comments/gfns0e/b271_oranga_tamariki_youth_court_age_amendment/
final_reading: reddit.com/r/ModelNZParliament/comments/gjguuc/b271_oranga_tamariki_youth_court_age_amendment/
amends:
- Oranga Tamariki Act 1989
---

# Oranga Tamariki (Youth Court Age) Amendment Act 2020

**1. Title**

This Act is the Oranga Tamariki (Youth Court Age) Amendment Act 2020.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

This Act amends the Oranga Tamariki Act 1989 (the **principal Act**).

**4. Section 2 amended (Interpretation)**

In section 2(1) of the Children, Young Persons and Their Families Act 1989, replace the definition of young person with:

***

> **young person** means a person of or over the age of 14 years but under 21 years and also has an extended meaning that includes some young adults for certain purposes under section 386AAA

***

**5. Section 248 amended (Family group conference not required in certain cases)**

Insert a new subsection (6) to read as follows:

***

> 6) Nothing in section 245(1)﻿(c) or section 246(b) or section 247(b) or (d) or (e) or section 281 requires a family group conference to be held in respect of any offence alleged or proved to have been committed by a young person if—
> 
> > a) the offence is alleged or proved to have been committed is of a physically violent or sexual nature; and
> > 
> > b) in undertaking any consultation under section 250, any victim, or representative of a victim, of the offence or alleged offence to which the conference relates has indicated that holding such a conference would be traumatic or harmful otherwise.

***

**6. Section 272 amended (Jurisdiction of Youth Court and children’s liability to be prosecuted for criminal offences)**

Subsections (1), (1A), (1B), (2), and (2A) are repealed.

**7. Section 386AAA amended (Interpretation)**

In section 386AAA, replace the definition of young person with:

***

> **young person** means a young person within the meaning given in section 2(1) and, for the purposes of sections 386A and 447(1)(cc) and (da), includes a young adult you is aged 21 years or over but under 25 years.

***

**8. Section 386AAD amended (Young persons entitled to live with caregiver up to age of 21 years)**

In section 386AAD(1), “(as defined in section 386AAA)” is deleted.

**9. Section 386C amended (Chief executive to maintain contact with young persons up to age of 21 years)**

In section 386C(1), “(as defined in section 386AAA)” is deleted.

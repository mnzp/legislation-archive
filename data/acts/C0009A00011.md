---
title: Domestic Violence (Protection of Pets) Amendment Act 2019
bill: 222
introducer: CrusherCollins2020
author: 
- Gregor_The_Beggar (ACT)
party: ACT
portfolio: Social Development
assent: 2019-12-06
commencement: 2019-12-07
first_reading: reddit.com/r/ModelNZParliament/comments/dxizlc/b222_domestic_violence_protection_of_pets/
committee: reddit.com/r/ModelNZParliament/comments/e0edj8/b222_domestic_violence_protection_of_pets/
final_reading: reddit.com/r/ModelNZParliament/comments/e3acj2/b222_domestic_violence_protection_of_pets/
amends:
- Domestic Violence Act 1995
---

# Domestic Violence (Protection of Pets) Amendment Act 2019

**1. Title**  

This Act is the Domestic Violence (Protection of Pets) Amendment Act 2019.

**2. Commencement**  

1) This Act comes into force on the day after the date on which it receives the Royal assent. 

**3. Purpose**  

This Act’s purpose is to allow the courts to offer protection orders towards animals in an abusive relationship where domestic violence has occurred. 

**4. Principal Act**

The **Principal Act** refers to Domestic Violence Act 1995

**5. Interpretation**

**Pets** means a registered animal which resides with the applicant and has had both an emotional connection to the applicant and has resided alongside them for a period of time.

**6. Section 14 Amended**

Every specific example of the phrase “the applicant, or a child of the applicant’s family, or both;” is hereby replaced with “The applicant, or a child of the applicant family, or a pet who has common company with the applicant’s family, or all three;”

**7. Section 16 Amended**

Section 16(1) amended to read as follows:

***

> “Where the court makes a protection order, that order applies for the benefit of any child or pet of the applicant’s family or household.”

***

Section 16(1A) amended to read as follows:

***

> A protection order continues to apply for the benefit of a child or pet of the applicant’s family until—
> 
> > a) the child or pet ceases to be a child or pet of the applicant’s family; or
> > 
> > b) the order sooner lapses or is discharged.

***
 
Section 16(1B) amended to read as follows:

***
 
> If a child of the applicant’s family having attained the age of 17 years continues to ordinarily or periodically reside with the applicant or a pet continues to ordinarily or periodically reside with the applicant (an adult child or pet), a protection order continues to apply for the benefit of the adult child or pet until—
> 
> > a) the adult child or pet ceases to ordinarily or periodically reside with the applicant; or
> > 
> > b) the order sooner lapses or is discharged.

***

Section 16(2) amended to read as follows:

***
 
> Subject to subsection (3), where the court makes a protection order, it may direct that the order also apply for the benefit of a particular person or pet with whom the applicant has a domestic relationship.

***
 
Section 16(5)(a) amended to read as follows:

***
 
> a child or pet who at the time of the applicant’s death was a child or pet of the applicant’s family, until that child attains the age of 17 years or, in the case of the pet, until the order lapses or is discharged; and

***
 
Section 16(5)(b) amended to read as follows;

***
 
> a person or pet in respect of whom a direction has been made under subsection (2), until the order lapses or is discharged.

***

---
title: Income Tax (Budget Measures May - July 2018) Amendment Act 2018
introducer: Fresh3001
party: ACT
portfolio: Finance
assent: 2018-05-02
urls:
- "https://reddit.com/r/ModelNZParliament/comments/8eeh7e/b44_income_tax_budget_measures_may_july_2018/"
amends:
- Income Tax Act 2007
---

#Income Tax (Budget Measures: May - July 2018) Amendment Act 2018

**Purpose**

The purpose of this Act is to amend the Income Tax Act 2007 to reflect the changes made in the May - July 2018 budget, including the extension of the Tax Free Threshold, and the implementation of a Small Business Tax.

**1. Title**

This Act is the Income Tax (Budget Measures: May - July 2018) Amendment Act 2018.

**2. Commencement**

This Act comes into force on the date at which the May - July 2018 budget is passed.

**3. Principal Act amended**

This Act amends the Income Tax Act 2007.

## Part 1 - Extension of the Tax Free Threshold

**4. Schedule 1 Part A Amended**

1) Replace Section 1 (a) with the following:

***

<table>
  <thead>
    <tr>
      <th><b>Row</b></th>
      <th><b>Range of dollar in taxable income</b></th>
      <th><b>Tax rate</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>$0 - $8,000</td>
      <td>0</td>
    </tr>
    <tr>
      <td>2</td>
      <td>$8,001 - $14,000</td>
      <td>0.085</td>
    </tr>
    <tr>
      <td>3</td>
      <td>$14,001 - $48,000</td>
      <td>0.15</td>
    </tr>
    <tr>
      <td>4</td>
      <td>$48,001 - $70,000</td>
      <td>0.25</td>
    </tr>
    <tr>
      <td>5</td>
      <td>$70,001 upwards</td>
      <td>0.30</td>
    </tr>
  </tbody>
</table>

***

## Part 2 - Small Business Tax

**4. Subpart YA 1 Amended**

1) Insert in the appropriate alphabetical order:

***

> **Small business** means any company, or any sole trader or member of a partnership who derives more than $48,000 in annual income from that business, which— 
> 
> > (a) employs no more than 19 individuals and;
> > 
> > (b) receives no more than $500,000 in annual income.

***

**5. Schedule 1 Part A Amended**

1) New section 3 (Taxable incomes: small businesses) inserted:

***

> To the extent to which a small business does not have a basic rate under clauses 4 to 10, the basic rate of income tax for a small business on each dollar of the business’s taxable income is 0.15.

***

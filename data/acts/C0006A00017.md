---
title: Sentencing (Home Detention Reintegration) Amendment Act 2019
bill: 136
introducer: ARichTeaBiscuit
author:
- ARichTeaBiscuit (Greens)
party: Greens
portfolio: Justice
assent: 2019-04-06
commencement: 2019-04-07
first_reading: reddit.com/r/ModelNZParliament/comments/b2duhf/b136_sentencing_home_detention_reintegration/
committee: reddit.com/r/ModelNZParliament/comments/b4u18y/b136_sentencing_home_detention_reintegration/
final_reading: reddit.com/r/ModelNZParliament/comments/b7jjcm/b136_sentencing_home_detention_reintegration/
amends:
- Sentencing Act 2002
---

# Sentencing (Home Detention Reintegration) Amendment Act 2019

**Explanatory note**

This Bill would allow certain prisoners to apply for the remainder of their sentence to be commuted to home detention when the prisoner has less than 12 months remaining before their release date (if the sentence is less than 24 months) or their parole eligibility date.

At present only offenders sentenced to less than 24 months imprisonment are granted leave to apply for home detention.

Prisoners who are convicted of a serious violent of sexual offence, or are serving a life sentence or preventative detention would not be eligible to apply.

Home detention allows offenders to access more rehabilitation and treatment programmes and training.

This Bill also would make it such that leave to apply for home detention is granted by default at sentencing, unless the judge orders otherwise. This is the reverse of the current system. Home detention is a sentence that should be used unless good reasons exist otherwise.

**1. Title**

This Act is the Sentencing (Home Detention Reintegration) Amendment Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

This Act amends the Sentencing Act 2002 (the principal Act).

**4. New section 80AA inserted (Interpretation)**

After the heading to subpart 2A of Part 2, insert: 

***

> **80AA. Interpretation**
> 
> In this subpart, unless the context otherwise requires,- 
> 
> **long-term determinate sentence** means a sentence of imprisonment that is for a fixed term and is a long-term sentence (as defined in section 4(1) of the Parole Act 2002) 
> 
> **parole eligibility date** has the same meaning as in section 4(1) of the Parole Act 2002 
> 
> **serious violent offence** has the meaning given to it in section 86A.

***

**5. Section 80I replaced (Leave to apply for cancellation of sentence for imprisonment and substitution of sentence for home detention in certain cases)**

Replace section 80I with: 

***

> **80I. Leave to apply for cancellation of sentence for imprisonment and substitution of sentence for home detention**
> 
> 1) This section applies if- 
> 
> > a) a court has sentenced an offendor to a short-term sentence of imprisonment; and 
> > 
> > b) the offence for which the sentence of imprisonment was imposed is not a serious violent offence. 
> 
> 2) At the time of sentencing, the court must make an order granting the offender leave to apply to the court of first instance at any time in the 12 month period immediately before the offender’s parole eligibility date, for cancellation of the remaining sentence of imprisonment and substitution of a sentence of home detention.

***

**6. New section 80IA inserted (Leave to apply for cancellation of long-term determinate sentence of imprisonment and substitution of sentence for home detention)**

After section 80I, insert: 

***

> **80IA. Leave to apply for cancellation of long-term determinate sentence of imprisonment and substitution of sentence for home detention** 
> 
> 1) This section applies if- 
> 
> > a) a court has sentenced an offendor to a long-term sentence of imprisonment; and 
> > 
> > b) the offence for which the sentence of imprisonment was imposed is not a serious violent offence. 
> 
> 2) At the time of sentencing, the court must make an order granting the offender leave to apply to the court of first instance at any time in the 12 month period immediately before the offender’s parole eligibility date, for cancellation of the remaining sentence of imprisonment and substitution of a sentence of home detention.

***

**7. Section 80J amended (Appeal against order granting leave to apply for cancellation of sentence of imprisonment and substitution of sentence of home detention)**

In section 80J, after "80I", insert ", "80IA," in each place.

**8. Section 80K amended (Application for cancellation of sentence of imprisonment and substitution of sentence for home detention)**

1) After section 80K(1), insert: 

***

> 1A) An offender who is subject to a long-term determinate sentence, and who has leave to apply for cancellation of the remaining sentence and substitution of a sentence of home detention under section 80IA, may apply to the court no earlier than 12 months before the offender’s parole eligibility date.

***

2) In section 80K(4), after "(1)", insert "or (1A)".

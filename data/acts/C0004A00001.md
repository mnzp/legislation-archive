---
title: Goods and Services Tax (Basic Foods) Amendment Act 2018
bill: 66
introducer: Timewalker102
party: Reform
portfolio: Finance
assent: 2018-08-07
commencement: 2018-08-08
urls:
- "https://reddit.com/r/ModelNZParliament/comments/91zai6/b66_goods_and_services_tax_basic_foods_amendment/"
amends:
- Goods and Services Tax Act 1985
---

#Goods and Services Tax (Basic Foods) Amendment Act 2018

**Purpose**

The purpose of this Act is to make all basic foods grown, produced, or collected in New Zealand zero-rated (effectively exempt) from GST on their sale. This would ensure greater access to basic foods for all New Zealanders, encourage healthy eating, and improve sales of New Zealand made basic foods.

**1. Title**

This Act is the Goods and Services Tax (Basic Foods) Amendment Act 2018.

**2. Commencement**

This Act comes into force on the day after the date on which this Act receives the Royal assent.

**3. Principal Act**

This Act amends the Goods and Services Tax Act 1985 (the **principal Act**).

**4. Section 2 amended (Interpretation)**

1) In section 2(1), insert in its appropriate alphabetical order:

***

> **basic food items** includes any food grown, produced, or collected in New Zealand that is solely—
> 
> > (a) milk, cheese, or any milk product that resembles milk except that it is made to comply with dietary requirements, such as soy or almond milk;
> > 
> > (b) fruits or vegetables;
> > 
> > (c) rice, oats, barley, or bread;
> > 
> > (d) eggs, chicken, or beef; or
> > 
> > (e) water.
>
> **food** has the same meaning as in the Food Act 2014.

***

**5. Section 11 amended (Zero-rating of goods)**

1) After section 11(1)(p), insert:

***

> (q) the goods are basic food items—
> > (i) which are sold and supplied to a recipient in New Zealand; and
> > (ii) which are not secondhand goods.

***

2) After section 11(8D), insert:

***

> (8E) Subsection (1)(q) does not apply if, as judged by the Commissioner, the supplier or vendor knowingly and intentionally increases the selling price of the goods to resemble or replace GST.

***

---
title: New Zealand Environmental Bill of Rights Act 2019
bill: 122
introducer: imnofox
author:
- KatieIsSomethingSad (Labour)
party: Greens
portfolio: Environment
assent: 2019-03-09
commencement: 2019-07-09
first_reading: reddit.com/r/ModelNZParliament/comments/as66uv/b122_new_zealand_environmental_bill_of_rights/
committee: reddit.com/r/ModelNZParliament/comments/augfyz/b122_new_zealand_environmental_bill_of_rights/
final_reading: reddit.com/r/ModelNZParliament/comments/awqex8/b122_new_zealand_environmental_bill_of_rights/
---

# New Zealand Environmental Bill of Rights Act 2019

**1. Title**

This Act is the New Zealand Environmental Bill of Rights Act 2019.

**2. Commencement**

This Act comes into force the day that is a four months after it receives royal assent.

**3. Interpretation**

In this Act, unless another intention appears in the Act, the following definitions are used--

**entitled person** has the meaning given to it in section 7. 

**resident** means any person who is currently living in New Zealand. 

**4. Minister responsible**

The Minister of the Crown responsible for this Act and the enforcement of the rights in it is that Minister who, under the authority of any warrant or with the authority of the Prime Minister, is for the time being responsible for the administration of this Act. 

**5. Rights affirmed**

The rights and freedoms contained in this Act are affirmed.

**6. Interpretation consistent with the Act to be preferred**

Wherever an enactment can be given a meaning that is consistent with the rights and freedoms contained in Act, that meaning shall be preferred to any other meaning

> a) Section 5 is exempted in the case that the meaning an enactment can be given that is consistent with this Act contradicts any treaty or agreement, which the New Zealand Government has agreed to, that specifically concerns the rights of Māori peoples. However, if a meaning which is consistent with both the Act and the treaty or agreement in question, that meaning shall be preferred to any other meaning. 

**7. Persons entitled to rights in this Act**

Entitled persons are those persons entitled to rights in this Act. These persons are every resident of New Zealand. 

> a) Nothing in this section or Act shall be construed as preventing non-residents from being afforded the rights in this Act.

**8. Government has obligation to enforce rights**

The Government of New Zealand has obligation to enforce and protect the rights afforded to entitled persons in this Act. 

**9. Right to clean environment**

All entitled persons have the right to enjoy a healthy and fairly clean environment. 

> a) In this section, "fairly clean" shall not be construed to only mean entirely without pollutants and other attributes determined to be unclean. 

**10. Right to clean drinking water**

All entitled persons have the right to clean and healthy drinking water suitable for human consumption. 

**11. Right to environmental protection**

All entitled persons have the right to clean, protect, and generally improve the environment of New Zealand. 

**12. Attorney-General as advisor for Act**

1) The Attorney-General, or if the position of Attorney-General is not filled a Minister of the Crown who, under the authority of any warrant or with the authority of the Prime Minister, is for the time being responsible for the administration of this section, is responsible to periodically review enactments in order to examine if they violate the rights in this Act. 

2) Should the Attorney-General, or the minister referred to in (1) if the Attorney-General position is vacant, determine that an enactment violates this Act, the Attorney-General must advise the Prime Minister and the government as a whole of this. 

3) The Government of New Zealand must create a means for entitled persons to request reviews of specific enactments under this section.

---
title: Appropriation (February - April 2018 Estimates) Act 2018
introducer: fartoomuchpressure
party: Labour
portfolio: Finance
assent: 2018-02-07
urls:
- "https://reddit.com/r/ModelNZParliament/comments/7uthde/b25_appropriation_februaryapril_2018_estimates/"
---

#Appropriation (February - April 2018 Estimates) Act 2018

**1. Title**

This Act is the Appropriation (February - April 2018 Estimates) Act

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Application**

The Act applies to the January - April 2018 financial year, which will end on the 30th April 2018.

**[Schedule 1 - Appropriations for the February - April 2018 financial year](https://docs.google.com/document/d/e/2PACX-1vRWzVUaLv2ig90Y508v_dKMvrBlm_58ZRUYhgWtEygJaIq2azV64P5kL9UYqPwIRLpwxp1EzZfJSyx8/pub)**

---
title: Education (Social Investment Funding and Abolition of Decile System) Amendment Act 2019
bill: 182
introducer: LeChevalierMal-Fait
author: 
- Erica Stanford MP (IRL figure)
party: ACT
portfolio: Education
assent: 2019-08-26
commencement: See section 2
first_reading: reddit.com/r/ModelNZParliament/comments/crxu1v/b182_education_social_investment_funding_and/
committee: reddit.com/r/ModelNZParliament/comments/cnyg2f/b182_education_social_investment_funding_and/
final_reading: reddit.com/r/ModelNZParliament/comments/clfjdt/b182_education_social_investment_funding_and/
amends:
- Education Act 1989
- Official Information Act 1982
---

# Education (Social Investment Funding and Abolition of Decile System) Amendment Act 2019

**1. Title**

This Act is the Education (Social Investment Funding and Abolition of Decile System) Amendment Act 2019.

**2. Commencement**

1) Subject to subsections (2) and (3), this Act comes into force on the day after the date on which it receives the Royal assent.

2) Sections 5 and 6 come into force following the passage of the next Appropriation Act.

**3. Principal Act**

This Act amends the Education Act 1989 (the **principal Act**).

**4. Section 2 amended (Interpretation)**

In section 2(1), replace “Parts 2, 3, and 11” with “Parts 2, 3, 8AA, and 11” in each place.

**5. New Part 8AA inserted**

After section 78T, insert:

***

> ## Part 8AA - Public funding of schools based on needs index
> 
> **79AA. Interpretation**
>
> In this Part, unless the context otherwise requires,—
>
> **decile system** means the mechanism referred to as any of the following:
> 
> > a) the mechanism established by the Ministry of Education in 1995 that uses census-based measures of socio-economic status:
> > 
> > b) the measure of the socio-economic position of a school’s student community relative to other schools throughout the country:
> > 
> > c) the measure of socio-economic status used to target funding and support schools:
> > 
> > d) the mechanism used to determine any of the following operational grants for schools:
> > 
> > > i) Targeted Funding for Educational Achievement:
> > > 
> > > ii) the Special Education Grant:
> > > 
> > > iii) the Careers Information Grant
>
> **NCEA Level 2** has the same meaning as in section 3(1) of the Social Security Act 1964
risk of under-achievement, in relation to a student, means that he or she is at risk of not achieving NCEA Level 2
>
> **needs index** means the needs index referred to in section 79AD
>
> **need indicator**, in relation to a student, has the meaning given in section 79AC
>
> **Statistician** has the same meaning as in section 2 of the Statistics Act 1975.
>
> *Abolition of decile system as funding basis*
> 
> **79AB. Abolition of decile system as basis for funding**
> 
> The Crown must not use the decile system to allocate any public money appropriated by Parliament for any purpose; for example, to fund operational grants for schools.
>
> *Needs index of schools*
>
> **79AC. Need indicator**
> 
> A student has a need indicator if he or she has 1 or more of the indicators prescribed by regulation that indicate that a student is at risk of under-achievement.
>
> **79AD. Needs index**
> 
> 1) The needs index is a calculation prescribed by regulation that is based on data regarding—
>
> > a) the total number and proportion of domestic students enrolled in a State school, State integrated school, or partnership school kura hourua who have need indicators; and
> > 
> > b) the weighting and rank of the need indicators.
>
> 2) The purpose of the needs index is to use it to target funding for students at risk of under-achievement.
>
> 3) The Minister must—
>
> > a) review and update the need indicators annually; and
> > 
> > b) calculate the needs index each financial year before allocating grants or supplementary grants under section 79; and
> > 
> > c) in accordance with the purpose in subsection (2), distribute a proportion of grants or supplementary grants to be paid under section 79 to the school’s board or sponsors.
>
> *Review of distribution of funding*
>
> **79AE. Request for review of funding distribution**
> 
> The chairperson of a board or a sponsor may request the Minister to review the distribution of funding for the school under section 79AD(3)﻿(b).
>
> **79AF. Minister to review funding distribution**
> 
> 1) If a request is received under section 79AE, the Minister must, within 60 days, review the distribution of funding for the school paid to the board or sponsor under section 79AD(3)﻿(b).
>
> 2) The Minister may allow officials nominated by the Minister to have access to the relevant needs index calculation and the data collected for it, for the purpose of reviewing the distribution of funding.
>
> *Privacy and needs index*
>
> **79AH. Compliance with Privacy Act 1993**
> 
> The Minister must ensure that collecting data for calculating the needs index (including need indicators of a student or students), storing the data, and calculating the needs index are done in compliance with the Privacy Act 1993.
>
> **79AI. Privacy code of practice**
> 
> 1) The Secretary, in consultation with the Privacy Commissioner appointed under the Privacy Act 1993, must, within 3 months after the commencement of this section, issue a code of practice that applies in respect of obtaining and using information or documents to determine the needs index.
>
> 2) The Secretary, and every officer of the Ministry acting under the delegation of the Secretary must comply with the code of practice.
>
> 3) The code of practice issued under this section is as if it were issued under section 46 of the Privacy Act 1993.
>
> 4) The Secretary may from time to time, in consultation with the Privacy Commissioner, amend the code of conduct, or revoke the code of conduct and issue a new code of conduct.
>
> *Needs index data not subject to Official Information Act 1982*
>
> **79AJ. Privacy of students**
> 
> The data collected for calculating the needs index (including need indicators of a student or students), the needs index calculation, and the needs index are not subject to the Official Information Act 1982.
>
> *Regulations*
>
> **79AK. Regulations relating to needs index system**
> 
> 1) The Governor-General may, by Order in Council on recommendation of the Minister, make regulations prescribing—
>
> > a) need indicators; and
> > 
> > b) the ranking and weight of the need indicators; and
> > 
> > c) the calculation by which the needs index is determined.
>
> 2) Before making a recommendation under subsection (1), the Minister must–
>
> > a) have regard to the purpose of the needs index; and
> > 
> > b) consult with the Statistician and any other persons and organisations that the Minister considers appropriate.

***

**6. Official Information Act 1982 amended**

1) This section amends the Official Information Act 1982.

2) In section 2(1), definition of official information, paragraph (l)﻿(ii), after “Observer”, insert “; and”.

3) In section 2(1), definition of official information, after paragraph (l), insert:

***

> m) does not include any information held by the Minister of Education or the Ministry of Education concerning—
>
> > i) data collected for calculating the needs index (including need indicators of a student or students), the needs index calculation, or the needs index for any school; or
> > 
> > ii) the amount of any grants or supplementary grants allocated to a board or sponsor under section 79AD(3)﻿(b).

***

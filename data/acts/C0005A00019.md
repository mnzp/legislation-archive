---
title: Conservation (Protection of Indigenous Freshwater Fish) Amendment Act 2019
bill: 108
introducer: imnofox
party: Greens
portfolio: Environment
assent: 2019-02-10
commencement: 2019-02-11
first_reading: reddit.com/r/ModelNZParliament/comments/a5gjjx/b108_conservation_protection_of_indigenous/
committee: reddit.com/r/ModelNZParliament/comments/acqebk/b108_conservation_protection_of_indigenous/
final_reading: reddit.com/r/ModelNZParliament/comments/amxfl7/b108_conservation_protection_of_indigenous/
amends:
- Conservation Act 1987
- Freshwater Fisheries Regulations 1983
---

# Conservation (Protection of Indigenous Freshwater Fish) Amendment Act 2019

**1. Title**

The title of this Act is the Conservation (Protection of Indigenous Freshwater Species) Amendment Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Purpose**

The purpose of this bill is to-

> a) improve the workability of fisheries management tools in the Conservation Act 1987:
> 
> b) fill gaps in the regulation-making powers relating to freshwater fisheries management:
> 
> c) provide protection for indigenous freshwater fish within conservation areas.

**3. Principal Act**

This Act amends the Conservation Act 1987 (the **principal Act**).

**4. Section 2 amended (Interpretation)**

1) In section 2(1), definition of **freshwater fish**, replace "fresh water" with "freshwater (but not any part of that water that is seawater)".

2) In section 2(1), repeal the definition of **indigenous fish**

3) In section 2(1), insert in their appropriate alphabetical order:

***

> **indigenous freshwater fish** means any freshwater fish that is indigenous to New Zealand
> 
> **Treaty settlement legislation** means an Act that settles the historical claims of iwi or other Māori groups under the Treaty of Waitangi and includes any regulations, bylaws, or other legislative instruments made under that Act

***

**5. Section 26ZL amended (Restrictions on fishing)**
After section 26ZL(1)(a), insert:

***

> aa) declare any specified land to be spawning grounds for freshwater fish and prohibit or impose restrictions and conditions on entry on to that land:

***

**6. New section 26ZLA inserted**

After section 26ZL, insert:

***

> **26ZLA. Restrictions on taking of indigenous freshwater fish**
> 
> 1) A person must not take any indigenous freshwater fish from a conservation area unless authorised under this section.
> 
> 2) A person may take indigenous freshwater fish from freshwater that is not a conservation area or part of a conservation area only if-
> 
> > a) the person is authorised under this section; or
> > 
> > b) the person takes the fish-
> > 
> > > i) primarily as food for human consumption, including for sale as food for human consumption; and
> > > 
> > > ii) in accordance with any regulations or notice made under this Act; and
> > 
> > c) the fish is not a species listed in schedule 5; or
> > 
> > d) the fish are-
> > 
> > > i) taken in a manner that does not lead to their injury or death; and
> > > 
> > > ii) returned to those waters ass soon as practicable after being taken.
> 
> 3) A person who contravenes subsection (1) or (2) commits an offence and is liable to a fine not exceeding $5,000.
> 
> 4) The Director-General may, on application, authorise a person to take indigenous freshwater fish-
> 
> > a) from a conservation areas if satisfied that the activity is consistent with the purpose for which the land is held and any requirements in regulations have been met; or
> > 
> > b) from a freshwater area that is not a conservation area or part of a conservation area if satisfied that the activity there will be no adverse effects on the affected fish population and any requirements in regulations have been met.
> 
> 5) An authorisation may be subject to any conditions or restrictions specified by the Director-General or in regulations.

***

**7. New section 26ZLB inserted**

After new section 26ZLA, insert:

***

> **26ZLB. Approval under specified Acts to access public land is not approval for recreational fishing of indigenous freshwater fish**
> 
> 1) An approval under this Act or an Act listed in Schedule 1 that enables access to public land for recreation purposes is not of itself an approval for recreational fishing of indigenous freshwater fish in any fisheries water on that land.
> 
> 2) In subsection (1), an approval under an Act includes an approval under any instrument made under that Act.

***

**8. Section 26ZJ amended (Offences relating to spawning fish)**

1) In section 26ZJ(1), after "who" insert "without authorisation".

2) In section 26ZJ(1)(c) replace "is in" with "takes or has".

3) Replace section 26ZJ(2) with:

***

> 2) Subsection (1) does not apply to the taking of freshwater fish subsequently found to contain eggs or larvae.
> 
> 3) In this section, authorisation means,-
> 
> > a) in relation to subsection (1)(a), an authorisation in regulations; and
> > 
> > b) in relation to subsection (1)(b) to (e), an authorisation from the Director-General under subsection (4).
> 
> 4) The Director-General may, on application, authorise a person to carry out an activity that would otherwise contravene 1 or more of paragraphs (b) to (e) of subsection (1) if satisfied that-
> 
> > a) the person cannot reasonably avoid the contravention when carrying out the activity; and
> > 
> > b) the activity is unlikely to have an adverse effect on the affected fish population.
> 
> 5) An authorisation may be subject to any conditions or restrictions specified in the authorisation.

***

**9. Section 26ZM amended (Transfer of release of live aquatic life)**

1) In section 26ZM(2)(a), replace "sites" with "locations".

2) In section 26ZM(4)(a), replace "shall" with "must, unless subsection (4A) applies,".

3) After section 26ZM(4), insert:

***

> 4A) The Director-General may, at the request of the applicant, determine that compliance with subsection (4) is not required if satisfied that the proposed activity is unlikely to have an adverse effect on the freshwater fishery concerned.

***

**10. Section 26ZP amended (Determination of closed seasons for fishing)**

1) Replace section 26ZP(1) to (3) with:

***

> 1) The Director-General may, by notice,-
> 
> > a) determine a closed season for fishing 1 or more species of freshwater fish (other than sports fish) in an area for a period that the Director-General has determined appropriate for the fish life history and threatened status; or
> > 
> > b) extend or vary a determination or vary a determination that has been extended.
> 
> 2) A notice-
> 
> > (a) must state the purpose of the closed season, the species of fish and the area to which the closed season applies, and the duration of the closed season; and
> > 
> > b) does not take effect until the Director-General has published the notice for at least 2 consecutive Saturdays in 1 or more of the daily newspapers circulating in the area concerned.
> 
> 3) A person commits an offence who, without authorisation under this section, takes, possesses, or in any way injures or disturbs a fish to which a closed season applies.

***

2) In section 26ZP(5), replace "sports fish" with "fish".

3) After section 26ZP(5), insert:

***

> 6) The Director-General may, on appliction, authorise a person to take fish to which a closed season applies if satisfied that-
> 
> > a) the taking of the fish is consistent with the purpose of the closed season; and
> > 
> > b) the person has met the requirements (if any) set out in regulations.
> 
> 7) An authorisation may be subject to any conditions or restrictions specified by the Director-General in regulations.

***

**11. Section 26ZR amended (Using hazardous substances to catch or destroy fish)**

Replace section 26ZR(2)(a) with:

***

> a) a fish and game ranger or an employee or a contractor of the Department; or

***

**12. Section 17J amended (Freshwater fisheries management plans)**

1) In section 17J(1) after "areas", insert "or throughout all New Zealand".

2) In section 17J(4) after "area", insert "or throughout all New Zealand".

3) After section 17J(5), insert:

***

> 6) However, if there is any conflict between a provision in a freshwater fisheries management plan and a provision in a sports fish and game management plan, the provision in the freshwater fisheries management plan prevails.

***

**13. Section 48A amended (Special regulations relating to freshwater fisheries)**

1) After section 48A(1)(n), insert:

***

> na) prohibiting, restricting, or regulating any structure or alteration to a water body that could impede or affect the passage of freshwater fish or specified freshwater fish:

***

2) After section 48A(1)(q), insert:

***

> r) in relation to indigenous freshwater fish,-
> 
> > i) specifying activities that are reasonably likely to injure or kill specified indigenous freshwater fish; and
> > 
> > ii) regulating, restricting, or imposing conditions on those specified activities; and
> > 
> > iii) specifying indigenous freshwater fish that are endangered and restricting or prohibiting the taking of those fish:
> 
> s) in relation to an authorisation by the Director-General under section 26ZLA, 26ZJ, or 26ZP,—
> 
> > i) stating any requirements that must be met by the person seeking the authorisation; and
> > 
> > ii) imposing conditions or restrictions on the authorisation:

***

**14. Section 26ZG amended (Application of Part)**

Replace section 26ZG(2)(c\) with:

***

> c) the taking, holding, possession, sale, or disposal of freshwater fish by—
> 
> > i) a person who is specifically authorised under the Fisheries Act 1983, the Fisheries Act 1996, or any regulations made under either of those Acts; or
> > 
> > ii) a person who is specifically authorised under any regulations made under section 48B of this Act; or
> > 
> > iii) a person acting under the authority of a registration of a fish farmer under Part 9A of the Fisheries Act 1996; or
> > 
> > iv) a person who is authorised (whether generally or specifically) by or under Treaty settlement legislation.

***

**15. New section 64C inserted**

After section 64B, insert:

***

> **64C. Power to amend Schedule 5**
> 
> 1) The The Governor-General may, by Order in Council made on the recommendation of the Minister, amend Schedule 5 to re-order species of threatened indigenous freshwater fish species, to add additional threatened indigenous freshwater fish species, or to remove indigenous freshwater fish species that are no longer threatened.
> 
> 2) The Minister must recommend that an Order in Council be made to amend Schedule 5, per subsection (1), on the advice of the Department of Conservation.

***

**16. New Schedule 5 inserted**

After Schedule 4, insert:

***

> ## Schedule 5: Threatened indigenous freshwater fish
> 
> Indigenous freshwater fish species that are threatened are---
> 
> **Nationally critical**
> 
> 1) Lowland longjaw galaxias (Galaxias cobitinis);
> 
> 2) Canterbury mudfish (Neochanna burrowsius);
> 
> 3) Lowland longjaw galaxias (Galaxias aff. cobitinis Waitaki");
> 
> 4) Clutha flathead galaxias (Galaxias "species D");
> 
> 5) Teviot flathead galaxias (Galaxias "Teviot");
> 
> **Nationally endangered**
> 
> 6) Central Otago roundhead galaxia (Galaxias anomalus);
> 
> 7) Eldon's galaxias (Galaxias eldoni);
> 
> 8) Dusky galaxias (Galaxias pullus);
> 
> 9) Alpine galaxias (Galaxias aff. paucispondylus "Manuherikia");
> 
> 10) Nevis galaxias (Galaxis "Nevis");
> 
> 11) Pomahaka galaxias (Galaxias "Pomahaka");
> 
> **Nationally vulnerable**
> 
> 12) Taieri flathead galaxias (Galaxius depressiceps);
> 
> 13) Gollum galaxias (Galaxias gollumoides);
> 
> 14) Bignose galaxias (Galaxias macronasus);
> 
> 15) Upland longjaw galaxias (Galaxias prognathus);
> 
> 16) Shortjaw kokopu (Galaxias postvectis);
> 
> 17) Lamprey (Geotria australis);
> 
> 18) Northland mudfish (Neochanna heleios);
> 
> 19) Alpine galaxias (Galaxias aff paucispondylus "Southland");
> 
> 20) Upland longjaw galaxias (Galaxias aff. prognathus "Waitaki");
> 
> 21) Northern flathead galaxias (Galaxias "northern").

***

**17. Amendments to Freshwater Fisheries Regulations 1983**

Amend the Freshwater Fisheries Regulations 1983 as set out in the Schedule.

## Schedule: Amendments to Freshwater Fisheries Regulations 1983

* Revoke Part 7.
* Revoke regulations 58 to 61.
* In regulation 62(1), replace "Notwithstanding regulation 61 no" with "No".
* Revoke regulation 63.
* In regulation 64(a), delete "“or any mosquito fish (Gambusia affinis)".
* Revoke regulations 70 and 71.
* In regulation 72(2), replace "51(1), 51(4), 57A, 57E(1), 58 to 66, 67B(2), and 68 to 71" with "57A, 57E(1), 62, 64 to 66, 67B(2), 68, and 69".

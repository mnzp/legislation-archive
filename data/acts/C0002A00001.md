---
title: Customs and Excise (Prohibition of Imports Produced by Slave Labour) Amendment Act 2018
introducer: imnofox
party: Greens
portfolio: Foreign Affairs
assent: 2018-02-02
urls:
- "https://reddit.com/r/ModelNZParliament/comments/7sgrb3/b11_customs_and_excise_prohibition_of_imports/"
amends:
- Customs and Excise Act 1996
---

#Customs and Excise (Prohibition of Imports Produced by Slave Labour) Amendment Act 2018

**1. Principal Act**

This Act amends the Customs and Excise Act 1996 (the **principal Act**).

**2. Purpose**

The purpose of this Act is to prohibit the import of goods made entirely or partially by slave labour.

**3. Section 2 amended (Interpretation)**

1) In section 2(1), insert in its appropriate alphabetical order:

***

> **slave labour** means labour by persons over whom any or all of the powers attaching to the right of ownership are exercised.

***

**4. Schedule 1 amended**

1) In Schedule 1, add:

***

> Goods manufactured or produced wholly or partially by slave labour, with exception to goods that have historical significance, or which could reasonably be determined as ‘antique’. 
>
> Exception may also be given to goods manufactured by slave labour should they be imported at behest of the person that directly produced them.

***

---
title: Minimum Wage (Flexible Minimum Wages) Amendment Act 2020
bill: 237
introducer: MerrilyPutrid
author:
- imnofox (Greens)
party: Greens
portfolio: Employment and Workforce
assent: 2020-02-27
commencement: 2020-02-28
first_reading: reddit.com/r/ModelNZParliament/comments/eu2730/b237_minimum_wage_flexible_minimum_wages/
committee: reddit.com/r/ModelNZParliament/comments/f76u1s/b237_minimum_wage_flexible_minimum_wages/
final_reading: reddit.com/r/ModelNZParliament/comments/fa7a57/b237_minimum_wage_flexible_minimum_wages/
amends:
- Education Act 1989
- C0009A00001
---

# Minimum Wage (Flexible Minimum Wages) Amendment Act 2020

**1. Title**

This Act is the Minimum Wage (Flexible Minimum Wages) Amendment Act 2020.

**2. Commencement** 

This Act comes into force the day after it receives Royal assent.

**3. Purpose**

The purpose of this Act is to allow for the minimum wage to be set more flexibly, on the advice of the Parliamentary Commissioner for the Labour Market.

**4. Principal Act**

This Act amends the Education Act 1989 (the **principal Act**).

**5. Section 4 replaced (Prescribed minimum adult rate of wages)**

Replace section 4 with the following:

***

> **4. Prescribed minimum adult rate of wages**  
> 
> 1) The Governor-General may, by Order in Council, prescribe a minimum adult rate of wages payable to workers—
> 
> > a) who are aged 16 years or older; and  
> > 
> > b) to whom any other minimum rate of wages prescribed under section 4A does not apply.  
> 
> 2) A rate prescribed under subsection (1) must be prescribed as a monetary amount.

***

**6. Section 4B replaced (Prescribed minimum training rate of wages)**

Replace section 4B with the following:

***

> **5. Prescribed minimum training rate of wages** 
> 
> 1) The Governor-General may, by Order in Council, prescribe a minimum training rate payable to 1 or more classes of workers who—
> 
> > a) are aged 16 years or older; and
> > 
> > b) are not involved in supervising or training other workers; and  
> > 
> > c) are employed under contracts of service under which they are required to undergo training, instruction, or examination (as specified in the order) for the purpose of becoming qualified for the occupation to which their contract of service relates.  
> 
> 2) A rate prescribed under subsection (1) must not be less than 80% of the minimum adult rate prescribed under section 4 and may be prescribed as—
> 
> > a) a monetary amount; or  
> > 
> > b) a percentage of the minimum adult rate.

***

**7. Section 5 amended (Annual review of minimum wages)**

Replace section 5(2) with the following:

***

> 2) Following a review under subsection (1), the Minister may, whether in that year or subsequently, with the advice of the Parliamentary Commissioner for the Labour Market, make recommendations to the Governor-General regarding the adjustments that should be made to that minimum rate.

***

**8. Transitional provision**

Section 28 of the Employment Relations (Liberalisation) Amendment Act 2019 is repealed.

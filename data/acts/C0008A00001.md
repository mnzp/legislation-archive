---
title: International Transparent Treaties (Repeal) Act 2019
bill: 181
introducer: LeChevalierMal-Fait
author: 
- FinePorpoise (National)
party: National
portfolio: Foreign Affairs
assent: 2019-08-26
commencement: 2019-08-27
first_reading: reddit.com/r/ModelNZParliament/comments/clfiwz/b181_international_transparent_treaties_repeal/
committee: reddit.com/r/ModelNZParliament/comments/cnyf23/b181_international_transparent_treaties_repeal/
final_reading: reddit.com/r/ModelNZParliament/comments/cqm6yv/b181_international_transparent_treaties_repeal/
repeals:
- C0005A00007
---

# International Transparent Treaties (Repeal) Act 2019

**1. Title**

This Act may be cited as the International Transparent Treaties (Repeal) Act 2019.

**2. Commencement**

This Act comes into force on the day after the date it receives Royal Assent.

**3. Purpose**

The purpose of this Act is to allow the executive to negotiate and agree to treaties.

**4. Repeal**

The International Transparent Treaties Act 2018 is repealed.

---
title: Bus Services Act 2020
bill: 256
introducer: TheOWOTringle
author:
- TheOWOTringle (Kiwi)
party: Kiwi
portfolio: Member's Bills
assent: 2020-03-26
commencement: 2020-03-27
first_reading: reddit.com/r/ModelNZParliament/comments/ff8g23/b256_bus_services_bill_first_reading/
committee: reddit.com/r/ModelNZParliament/comments/fie79n/b256_bus_services_bill_committee/
final_reading: reddit.com/r/ModelNZParliament/comments/ff8g23/b256_bus_services_bill_first_reading/
---

#Bus Services Act 2020

**1. Title**

This Act is the Bus Services Act 2020.

**2. Commencement**

This Act comes into force the day after it receives Royal assent.

**3. Purpose**

The purpose of this Act is to protect critical bus routes and invest in bus routes through rural areas.

**4. Definitions**

In this Act, unless otherwise expressly stated—

1) A **bus route** is defined as a fixed journey a bus makes to take passengers to different places.

2) A **village** is defined as to an inhabited settlement of at least 200 people.

3) **Protected by the government** is defined as

> a) funding shall be secured,
>
> b) the bus route shall not be closed, 
>
> c) usage of the bus route shall be available to everyone paying bus fees to use it. 

**6. Protecting Bus Routes**

1) A bus route which stops at a place of education shall be protected by the government. 

2) A bus route which sees at least 200 passengers a week shall be protected by the government.

**7. Investing in rural bus routes**

All villages must have a bus route serving them at least once every day excluding Sundays and National Holidays.

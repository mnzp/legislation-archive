---
title: Forestry Accounting Act 2019
bill: 127
introducer: FinePorpoise
author:
- FinePorpoise (National)
party: National
portfolio: Member's Bills
assent: 2019-04-03
commencement: 2019-04-04
first_reading: reddit.com/r/ModelNZParliament/comments/avmhu9/b127_forestry_accounting_bill_first_reading/
committee: reddit.com/r/ModelNZParliament/comments/b04ik7/b127_forestry_accounting_bill_committee/
final_reading: reddit.com/r/ModelNZParliament/comments/b6fe36/b127_forestry_account_bill_final_reading/
---

# Forestry Accounting Act 2019

**1. Title**

This Act is the Forestry Accounting Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Purpose**

The purpose of this Act is to ensure that the economic effects of the forestry industry are accounted for within the New Zealand National System of Accounts.

**4. Interpretation**

In this Act—

**account** means a publication part of the New Zealand National System of Accounts

**forest land** means land on which forest is growing, land used for agroforestry, and land intended to be forested or used for agroforestry by the owner or occupier of the land

**forestry satellite account** means collection of economic data on the forestry sector

**forestry sector** means all matters and activities concerned with or affecting the production and use of goods from or on all forest land that is devoted primarily to commercial production; and shall include protection from disease and fire of all forests, the protective effects of forests, the harvesting and processing of trees and other forest plants, and other matters associated with commercial forestry production

**market effects** means economic outputs produced in the forestry sector which are traded on the market as goods or services

**Minister** means the Minister of Business, Innovation, and Employment

**quasi-market effects** means those economic outputs produced through activities of the forestry sector directly contributing to the market value of market effects but not directly traded on the market as a good or service, such as ecological services

**System of Accounts** means the New Zealand National System of Accounts

**5. Establishing the forestry satellite account**

1) The Minister shall publish the forestry satellite account—

> a) on an annual basis in conjunction with other accounts
> 
> b) making it available in print and in digital formats to the public
> 
> c) with the intention that the forestry satellite account is a publication in the System of 	Accounts

**6. Data collection and presentation in the forestry satellite account**

1) The Minister shall publish the forestry satellite account so that the following data are included:

> a) direct and indirect value added from the forestry sector including market effects and quasi-market effects
> 
> b) direct and indirect value added from the forestry sector only including market effects
> 
> c) total expenditure within the forestry sector
> 
> d) direct and indirect employment within the forestry sector
> 
> e) forest land use and value of forest land
> 
> f) any other data fitting with the goals of an account in the System of Accounts

2) the Minister shall publish the data of past and present years in each report

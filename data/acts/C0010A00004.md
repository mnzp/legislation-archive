---
title: International Transparent Treaties Act 2020
bill: 239
introducer: ARichTeaBiscuit
author:
- Fletcher Tabuteau (IRL figure)
party: Greens
portfolio: Foreign Affairs
assent: 2020-02-17
commencement: 2020-02-18
first_reading: reddit.com/r/ModelNZParliament/comments/eu2890/b239_international_transparent_treaties_bill/
committee: reddit.com/r/ModelNZParliament/comments/f8mhfi/b239_international_transparent_treaties_bill/
final_reading: reddit.com/r/ModelNZParliament/comments/fc6y7a/b239_international_transparent_treaties_bill/
---

# International Transparent Treaties Act 2020

**1. Title**

This Act is the International Transparent Treaties Act 2020.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

## Part 1 - Preliminary provisions

**3. Purpose**

The purpose of this Act is to ensure that Parliament approves international treaties before they become binding.

**4. Interpretation**

In this Act, unless the context otherwise requires,—

**binding action**, in relation to a treaty or proposed treaty, means—

> a) signing the treaty to indicate an intent to be bound by it; or
> 
> b) fully executing it; or
> 
> c) ratifying it

**House** means the House of Representatives

**Minister** means the Minister of Foreign Affairs and Trade

**treaty** means an international agreement between subjects of international law (whether states or international organisations), in written form and governed by international law, whether embodied in a single instrument or in 2 or more related instruments and whatever its particular designation; and includes any amendment to a treaty.

**5. Act binds the Crown**

This Act binds the Crown.

## Part 2 - House to approve proposed treaty before binding action taken

**6. Minister to refer proposed treaty to House**

Before any binding action is taken on behalf of the Crown in relation to a proposed treaty, the Minister must present the text of the proposed treaty to the House to seek its approval of the proposed treaty.

**7. Approval of proposed treaty by House**

Unless the House approves a proposed treaty after it is referred to the House under section 6, no binding action may be taken on behalf of the Crown in relation to the proposed treaty.

**8. Incorporation of treaty into New Zealand law**

1) No treaty or proposed treaty has the force of law in New Zealand by reason only of the approval of the proposed treaty by the House after it is referred to the House under section 6.

2) A treaty or proposed treaty referred to the House under section 6 and approved by the House has the force of law in New Zealand only when it is incorporated into New Zealand law by an enactment.

---
title: Container Deposit Act 2019
bill: 123
introducer: imnofox
author:
- imnofox (Greens)
party: Greens
portfolio: Environment
assent: 2019-03-18
commencement: 2019-03-19
first_reading: reddit.com/r/ModelNZParliament/comments/atcqkt/b123_container_deposit_bill_first_reading/
committee: reddit.com/r/ModelNZParliament/comments/axuu7l/b123_container_deposit_bill_committee/
final_reading: reddit.com/r/ModelNZParliament/comments/b04e1q/b123_container_deposit_bill_final_reading/
---

# Container Deposit Act 2019

**1. Title**

This Act is the Container Deposit Act 2019.

**2. Commencement**

This Act comes into force the day after it receives the Royal Assent.

**3. Purpose**

The purpose of this Act is to increase recycling of various beverage containers, reducing waste in our landfills and streets.

**4. Interpretation**

In this Act, unless the context otherwise requires-

**accountable person** means the person who carries on the business of selling goods of products or, if two or more persons each carry on such a business in or at the particular premises, whichever of them sells one or more beverages in containers

**collection depot** means a collection depot established pursuant to section 7

**container** means a container of any kind made for the purpose of containing a beverage, being a container which when filled with the beverage is sealed for the purposes of storage, transport, and handling prior to its sale or delivery for the use or consumption of its contents

**DOC Community Fund** means the fund administered by the Department of Conservation that provides financial support to communituy-led conservation efforts on private and public land

**glass container** means a container made of glass whether solely or in combination with any other substance or thing

**refund amount** means an amount prescribed as the refund amount in relation to containers of that description

**5. Exemption of certain containers by regulation**

The Governor-General may, by regulation, exempt containers of a specified description from the application of this Act or specified provisions of this Act either unconditionally or subject to particular conditions.

**6. Container must be marked with refund amount**

1) An accountable person must not sell a beverage in a container unless the container-

> a) is marked in a manner and form approved by the Minister with a statement indicating the refund amount applicable to that container; and
> 
> b) if the Minister so requires, has some other mark or feature approved by the Minister to complement the marking made under clause (a).

2) Every accountable person commits an offence who sells a beverage in a container that is not marked per subsection (1).

3) An accountable person who commits an offence against subsection (2) is liable on conviction to a fine not exceeding $2000.

**7. Establishment of collection depots**

1) The Minister may by notice published in the *Gazette*-

> a) approve the establishment of a collection depot in relation to containers of a specified description or descriptions;
> 
> b) impose conditions as to the operation of that collection depot.

2) The Minister may by notice published in the *Gazette* amend any notice referred to in subsection (1) and upon that publication the notice as amended has effect according to its terms.

3) The Minister may by notice published in the *Gazette* revoke the approval of the establishment of a depot given under subsection (1).

**8. Deposit on containers**

1) The Minister may by notice published in the *Gazette* specify a refund amount (the "deposit") of no more than 50 cents.

2) A payment shall be applied, equivalent to the value of the desposit, upon to customers at the point of sale to them for each beverage in a container that they purchase.

3) An accountable person must pay the deposit to the government not later than the 20th day of the month following the end of each accounting period.

4) The Inland Revenue Department, or the appropriate successor department, is responsible for collecting this charge from any accountable person.

5) A full and true return in the form that the Minister specifies must be furnished by the accountable person which details the amount of the deposits payable during the accounting period.

6) The government must use revenue to reimburse collection depots for refunds made under section 9 along with a 3c handling fee for every bottle collected.

**9. Collection depots to pay refund amount for empty containers**

1) Except as provided in this section, the person in charge of a collection depot must-

> a) accept delivery of empty containers, marked per section 6, of a description in relation to which the establishment of that collection depot was approved; and
> 
> b) pay to the person delivering that container the refund amount applicable to that container.

2) A person referred to in subsection (1) may refuse any container that is not clean.

**10. Conscientious and religious objection**

1) An accountable person who objects on conscientious or religious grounds to paying the charge in the manner provided for may pay the amount concerned to the Director-General of the Inland Revenue Department.

2) The Director-General must pay the amount to central government.

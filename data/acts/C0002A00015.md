---
title: Citizenship (Honouring Te Tiriti in Oaths) Amendment Act 2018
introducer: KatieIsSomethingSad
party: Labour
portfolio: Internal Affairs
assent: 2018-03-24
urls:
- "https://reddit.com/r/ModelNZParliament/comments/85d7j5/b29_citizenship_honouring_te_tiriti_in_oaths/"
amends:
- Citizenship Act 1977
---

#Citizenship (Honouring Te Tiriti in Oaths) Amendment Act 2018

**1. Purpose**

The purpose of this Act is to have Te Tiriti o Waitangi and tangata whenua recognised in the oath of allegiance and to ensure new citizens are aware of Te Tiriti and the local iwi and hapū.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

1) This Act amends the Citizenship Act 1977 (the **principal Act**).

**4. Schedule 1 replaced (Oath of Allegiance)**

1) Replace Schedule 1 with the following:

***

> ###Schedule 1 - Oath of allegiance 
> 
> I, [full name], swear that I will be faithful and bear true allegiance to Tangata Whenua and the Crown, according to law, that I will honour Te Tiriti o Waitangi, and faithfully observe the laws of Aotearoa New Zealand and fulfil my duties as a citizen of Aotearoa New Zealand.

***

**5. Section 11 amended (Minister may require oath or affirmation of allegiance to be taken)**

1) After section 11(2), insert:

***

> 3) The person conducting the public citizenship ceremony must ensure the applicant is informed of—
> 
> > (a) Te Tiriti o Waitangi in the form specified in Schedule 1A ; and
> > 
> > (b) the hapū and iwi in the region the applicant resides in.

***

**6. New Schedule 1A inserted (Acknowledgement of Te Tiriti o Waitangi)**

1) After Schedule 1, insert:

***

> **Schedule 1A - Acknowledgement of Te Tiriti o Waitangi**
> 
> In becoming new citizens of New Zealand, you are joining a nation whose foundation is a Treaty between the indigenous tangata whenua (people of the land) and the Crown. This Treaty, Te Tiriti o Waitangi, offers citizens the opportunity to participate in the ongoing journey towards honourable relationships that are founded on the articles of Te Tiriti agreed to in 1840.

***

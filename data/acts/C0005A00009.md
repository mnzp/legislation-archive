---
title: Income Tax (Definition of Income) Act 2018
bill: 103
introducer: silicon_based_life
party: Opportunities
portfolio: Finance
assent: 2018-12-12
commencement: 2018-12-13
urgent_reading: reddit.com/r/ModelNZParliament/comments/a2se94/b103_income_tax_definition_of_income_amendment/
amends:
- Income Tax Act 2007
repeals:
- Capital Gains Tax Act 2018
---

# Income Tax (Definition of Income) Amendment Bill 

**1. Title**

This bill is titled the “Income Tax (Definition of Income) Act 2018”.

**2. Commencement**

This amendment comes into force on the day after it receives royal assent.

**3. Interpretation**

Definitions used in this bill are provided in Part B - Amendments to Part Y of the Principal Act.

**4. Principle Act Amended**

This bill amends the Income Tax Act 2007 (the **principal Act**).

## Part A - Amendments to Part C of the Principal Act

**5. Section CB 1 amended**

Amend section CB 1 (2) to read:

***

> (2) Subsections (1) and (3) do not apply to an amount that is of a capital nature except as specified in any section of subpart CB.

***

Add section CB 1 (3), directly after section CB (2), to read:

***

> *Foreign business clarification*
> 
> (3) For all foreign-owned businesses operating in New Zealand, the section of that business that interacts with the New Zealand domestic economy is liable to have that section of the business apply to subpart CB of this act, and thus of this act as a whole.

***

**6. Section CB 37 added**

Under Subpart CB of the Principal Act, add the following section CB 37 below the current section CB 36:

***

> **Taxable return on productive assets**
> 
> **CB 37 	Taxable return on productive assets**
> 
> *When this section applies*
> 
> This section applies when businesses hold productive assets that do not generate income in accordance with their business model.
> 
> *Taxable return on productive assets*
> 
> When the productive assets held by a business, out of the scope of their business model, generates capital income during an income year, the amount of income generated is taxable income for that business.
The taxable income applies to the person who owns the productive assets in question, either through business ownership or direct ownership.

***

**7. Section CC 1 Amended**

Under Subpart CC of the Principal Act, add the following section CC 14 below the current CC 13:

***

> **Taxable return on capital gains through property**
> 
> **CC 14 Taxable return on productive property assets**
> 
> *When this section applies*
> 
> This section applies when a financial entity purchases or sells any real estate property. 
> 
> *Price of real estate property*
> 
> The amount of money the financial entity spends to purchase the real estate property is, for the purposes of this section, called the purchase price.
> 
> The amount of money the financial entity receives from the sale of the real estate property is, for the purposes of this section, called the sale price.
> 
> *Income from real estate property*
> 
> The amount of real capital gain the financial entity receives from sale of the property shall be calculated by subtracting the purchase price from the sale price.
> 
> Before the subtraction occurs, the purchase price must be adjusted for inflation using Reserve Bank guidelines.
> 
> For each financial entity referred to in this section, there will be an associated person or persons who receives the capital gains from the property transaction in real terms. The person or persons may be the financial entity themselves.
> 
> The amount of real capital gain calculated in this section is then added to the total taxable income for that person or persons for the tax year during which the sale took place.
> 
> If the amount of real capital gain is below NZ $0, it is not added to their total taxable revenue stream.

***

## Part B - Amendments to Part Y of the Principal Act

**8. Section YA 1 amended**

Under the subpart heading, between the definitions of  “income from personal exertion” and “income interest”, insert the following:

***

> income from productive assets is defined as the level of assessable income that the productive assets owned by an entity generates

***

Under the subpart heading, between the definitions of “producer board” and “profit”, insert the following:

***

> productive assets is defined as any asset that generates capital or income

***

## Part C - Capital Gains Tax Act 2018 repealed

**9. Capital Gains Tax Act 2018 repealed**

The Capital Gains Tax Act 2018 is hereby repealed.

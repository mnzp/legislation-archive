---
title: Electoral (Māori Option) Amendment Act 2019
bill: 150
introducer: AnswerMeNow1
author:
- AnswerMeNow1 (Greens)
party: Greens
portfolio: Māori Affairs
assent: 2019-06-01
commencement: 2019-06-02
first_reading: reddit.com/r/ModelNZParliament/comments/bof149/b150_electoral_māori_option_amendment_bill_first/
committee: reddit.com/r/ModelNZParliament/comments/bqrsmm/b150_electoral_māori_option_amendment_bill/
final_reading: reddit.com/r/ModelNZParliament/comments/bt4gqz/b150_electoral_māori_option_amendment_bill_final/
amends:
- Electoral Act 1993
---

# Electoral (Māori Option) Amendment Act 2019

**1. Title**

This Act is the Electoral (Māori Option) Amendment Act 2019.

**2. Purpose**

The purpose of this bill is to enable Māori voters to change roll type at any time, per the recommendations of the Electoral Commission.

**3. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**4. Principal Act**

This Act amends the Electoral Act 1993 (the **principal Act**).

**5. Section 3 amended (Interpretation)**

In section 3, definition of Māori electoral population, paragraph (a)(i), replace “the last day of the period specified in the last notice published undersection 77(2)” with “the date determined by the Government Statistician”.

**6. Section 35 amended (Division of New Zealand into General electoral districts)**

1) Replace section 35(5) with:

***

> 5) The Electoral Commission must supply the Government Statistician with the information required under section 45(3A) on the date determined by the Government Statistician under that section.

***

2) In section 35(6)(b), replace “section 77(6), to supply to the Government Statistician as soon as practicable after the last day of the period specified in the notice published under section 77(2),” with “to supply under section 45(3A),”.

**7. Section 45 amended (Maori representation)**

After section 45(3), insert:

***

> 3A) For the purpose of enabling the Government Statistician to calculate the Māori electoral population, the Electoral Commission must, on a date determined by the Government Statistician, supply to the Government Statistician-  
> 
> > a) the total number of persons registered as electors of the Māori electoral districts as at the close of that date; and
> > 
> > b) the total number of persons registered as electors of the General electoral districts, who, as at the close of that date, are recorded as having given written notice to the Electoral Commission that they are persons of New Zealand Māori descent; and
> > 
> > c) the total number of persons whose names are shown on the dormant rolls maintained under section 109 for the Māori electoral districts; and
> > 
> > d) the total number of persons whose names are shown on the dormant rolls maintained under section 109 for General electoral districts who are recorded as having given written notice that they are persons of New Zealand Māori descent.

***

**8. Section 76 amended (Maori option)**

1) In section 76(1), replace “sections 77 to 79” with “sections 78 and 79”.

2) Delete section 76(2)(b).

3) In section 76(2)(c), delete “section 77 or”.

**9. Section 77 deleted (Periodic exercise of Maori option and determination of Maori electoral population)**

Delete section 77.

**10. Section 78 replaced (Exercise of Maori option)**

Replace section 78 with:

***

> **78. Exercise of Māori option**
> 
> 1) Every Māori person may exercise the option given by section 76(1) at any time by advising the Electoral Commission whether they wish to be registered as an elector of-  
> 
> > a) a General electoral district; or
> > 
> > b) a Māori electoral district.
> 
> 2) A Māori person who wishes to exercise the option under section 76(1) must do so in a form and manner approved by the Electoral Commission.
> 
> 3) A Māori person who is outside New Zealand, or who has a physical or mental impairment may exercise the Māori option through a representative, and section 86 applies with any necessary modifications.
> 
> 4) Advice received under subsection (1) is deemed to be an application for registration as an elector for the purposes of-  
> 
> > a) the definition of electoral roll in section 3(1); and
> > 
> > b) sections 89A, 98, and 103.
> 
> 5) A Māori person who receives a notice sent under section 78A but who does not exercise the option given in section 76(1) continues to be registered on the roll as an elector of the electoral district in which he or she is currently registered.
> 
> **78A. Electoral Commission to notify Māori of the option**
> 
> 1) The Electoral Commission must, at least once between each periodical census, send a notice to-  
> 
> > a) every person registered as an elector of a Māori electoral district; and
> > 
> > b) every person registered as an elector of a General electoral district who has,-  
> > 
> > > i) in his or her application for registration as an elector, specified that he or she is Māori; or
> > > 
> > > ii) in response to an inquiry under section 89D, notified the Electoral Commission that they are Māori.
> 
> 2) A notice under subsection (1) must-  
> 
> > a) contain particulars about whether the person to whom the notice is addressed is currently registered as an elector of a Māori electoral district or a General electoral district; and
> > 
> > b) advise the person to whom the notice is addressed that they may exercise the option under section 76(1); and
> > 
> > c) provide advice on the form and manner for exercising the option undersection 76(1).
> 
> 3) In this section, **person registered as an elector** includes a person of or over the age of 17 years who has had an application under section 82(2) to register as an elector accepted by the Electoral Commission.

***
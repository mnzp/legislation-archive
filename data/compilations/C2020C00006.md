---
principal: C0001A00001
upto: C0011A00004
---

# Zero Carbon Act 2017

**Title**

This Act is the Zero Carbon Act 2017. 

**Commencement**

This Act comes into force on the day after the date on which this Act receives the Royal assent.

**1. Purpose**

1) The purpose of this Act is:

> a) to require a reduction of net New Zealand carbon emissions to zero by 2050; and
>
> b) to establish an independent climate commission to oversee the government’s actions and provide advice; and
>
> c) to require governments to set five year carbon budgets at the beginning of each term; and
>
> d) to require governments to produce policy plans at the beginning of each term and to annually report on its progress.

**2. Interpretation**

In this Act—

**net zero carbon** means the target explained in section 3(1).

**net carbon account for a period** means the amount of net NZ emissions of targeted greenhouse gases for the period. 

**targeted greenhouse gases** means gases as listed under section 4(1).  

**long lived greenhouse gases** means gases as listed under section 4(1)(a).  

**short lived greenhouse gases** means gases as listed under section 4(1)(b). 

## Part 1: Target emissions

**3. Zero carbon target**

1) It is the duty of the Prime Minister to ensure that the net New Zealand carbon account for long lived greenhouse gases by the year 2035 is less than or equal to zero.

2) It is the duty of the Prime Minister to ensure that the net New Zealand carbon account for short lived greenhouse gases by the year 2035 is reduced to a sustainable amount.

**4. Targeted greenhouse gases**

1) "targeted greenhouse gases" means:

> a) Long lived greenhouse gases, which mean:
> 
> > i) carbon dioxide
> 
> > ii) nitrous oxide
> 
> > iii) hydrofluorocarbons with lifetimes of over 50 years
> 
> > iv) perfluorocarbons with lifetimes of over 50 years; or
> 
> > v) sulfur hexafluorides with lifetimes of over 50 years.

> b) Short lived greenhouse gases, which mean:

> > i) methane

> > ii) hydrofluorocarbons with lifetimes of under 50 years

> > iii) perfluorocarbons with lifetimes of under 50 years; or

> > iv) sulfur hexafluorides with lifetimes of under 50 years.

## Part 2: Achieving targets

**5. Carbon budgets**

1) It is the duty of the Minister responsible for Climate Change:

> a) to set for the following period of five years an amount for the net New Zealand carbon account for long lived gases and an amount for the net New Zealand carbon account for short lived gases (together the "carbon budget") at the beginning of each parliamentary term; and

> b) to ensure that the net New Zealand carbon account does not exceed the carbon budget.

2) The carbon budget may be set at any time that is no earlier than the 1st May and no later than the 20th June in the appropriate year.

3) the Minister responsible for Climate Change must consult with and enact the advice of the Independent Climate Commission when setting the carbon budget.

**6. Government plan**

1) The Minister responsible for Climate Change is required to present a single policy plan at the start of each parliamentary term to reduce domestic emissions and meet the zero carbon target.

2) The plan must cover:

> a) the government’s plan to reduce long lived gases
> 
> b) the government’s plan to reduce short lived gases; and
> 
> c) the government’s overall plan to meet carbon budgets; and
> 
> d) the government’s plan to increase renewable energy sources and reduce national dependency on energy sources that contribute to emissions, with an end-goal target of full dependence on renewable energy sources by **2035**; and  
> 
> e) the government’s intentions around support via subsidies for research and development into renewable energy sources; and  
>  
> f) the government’s intended tax rates on carbon emission by businesses and corporations; and  
>  
> g) the government’s intentions around reducing and replacing the number of publicly and privately owned carbon emitting vehicles and other modes of transport, including busses and light/heavy rail, with non-emitting or low-emission alternatives, with an end-goal target to eliminate or heavily reduce the number of carbon emitting vehicles by **2035**; and  
>  
> h) the government’s intentions around collaboration with farmers to eliminate or heavily reduce carbon emissions from the agricultural sector; and  
>  
> i) the government’s intentions around collaboration with manufacturers to eliminate or heavily reduce carbon emissions from the industrial manufacturing sector; and  
>  
> j) the government’s plan to ensure anybody made redundant in relation to anti-emission measures and their effects will be kept out of unemployment wherever possible.

3) The Minister responsible for Climate Change is required to annually report to Parliament the progress of the plan and the forecasts.

4) The Minister responsible for Climate Change must consult with iwi.

5) the Minister responsible for Climate Change should act on the advice given by the Independent Climate Commission during the planning process for the Carbon Budget and Policy.

6) if the Minister responsible for Climate Change does not act on this advice, they should provide a statement outlining why to the House of Representatives at the next possible opportunity.

**7. Independent Climate Commission**

1) An independent Climate Commission will be established with the purpose of:

> a) advising the government on targets and policies to put New Zealand on track to net zero carbon; and

> b) holding the government to account by publishing progress reports and highlighting problems.

2) The Climate Commission will be an Office of Parliament, meaning that:

> a) the commission will be appointed by and report to Parliament; and

> b) the commission can independently prepare reports without government oversight.

3) The Climate Commission will consist of eleven experts appointed by Parliament, with the aim of having a broad panel of experts wholly representing:

> a) agricultural science and practices

> b) business competitiveness

> c) climate and environmental science

> d) climate change policy

> e) economic analysis

> f) emissions trading

> g) energy

> h) Te Tiriti o Waitangi, tikanga Māori, and Māori interests; and

> i) employment, workforce, and labour rights.

4) Appointments to the independent Climate Commission must be consulted about with the Chair of the Climate Commission.

5) Members of subcommittees do not have to be appointed by Parliament or members of the Climate Commission.

6) The Climate Commission will be required to prepare non-binding advice on:

> a) long term targets, including:

> > i) net zero carbon by 2035; and

> > ii) reduction in short lived greenhouse gases.

> b) carbon budgets, including:

> > i) a recommended carbon budget; and

> > ii)instructions on how the budget can be achieved.

> c) other climate issues as requested by the government.

7) The Climate Commission will be required to produce the following two types of progress reports with the purpose of holding the government to account:

> a) annual progress reports, which include:

> > i) reporting on the government’s progress of meeting upcoming carbon budgets and targets

> > ii) highlighting the problems with the government’s approach; and

> > iii) advising the government on any further progress that is deemed needed.

> b) carbon budget reports, two years after a carbon budget ends, which include:

> > i) how the budget was or was not met

> > ii) what action was taken by the government to reduce emissions; and

> > iii) highlight any problems with the approach.
    
8) The Minister responsible for Climate Change should respond to and enact the advice of both reports. If they do not wish to follow this advice, they should follow the same process outlined in Section 6, subsection 6.

9) the Independent Climate Commission will set the figures on net carbon and gross carbon reduction targets over the 5 year period to be outlined in the termly government Carbon Budget and Policy Statement.

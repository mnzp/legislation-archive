---
principal: C0002A00002
upto: C0007A00022
---

#End of Life Choice Act 2018

**1. Purpose**

The purpose of this Act is to allow those suffering from a terminal illness or a grievous and irremediable medical condition the ability to request assisted dying. The Act will carefully define those who are eligible for assisted dying, and will detail a series of provisions that will ensure that the choice to request assisted dying is a free one, made without coercion by an individual who is mentally capable and understands the consequences of assisted dying.

**2. Interpretation**

In this Act, unless the context requires another meaning:

**assisted dying** means the administration by a medical practitioner of a lethal dose of medication to a person to relieve his or her suffering by hastening death.

**attending medical practitioner** means a person’s medical practitioner.

**competent** means having the ability described in section 3(e).

**independent medical practitioner** means a medical practitioner who is independent of an attending medical practitioner and the person.

**person who is eligible for assisted dying** has the meaning given to it in section 3.

**prescribed form** refers to a form which will be drafted by the the SCENZ group to be used to process an assisted dying request or confirmation, and will include the person’s signature confirming consent, and any other relevant details.

**replacement medical practitioner** means any medical practitioner who the SCENZ group will assign to act as the attending medical practitioner for any patient whose original medical practitioner is unwilling to complete the requirements of this Act.

**independent review committee** means the committee established under section 11(d).

**SCENZ** means Support and Consultation for End of Life in New Zealand.

**SCENZ group registrar** means the employee of the Ministry of Health appointed to compile a list of all assisted dying reports submitted to the SCENZ group.

**3. Definition of a person who is eligible for assisted dying**

1) In this Act, person who is eligible for assisted dying means a person who:

> a) is aged 18 years or older,
> 
> b) is a person who has New Zealand citizenship or residency,
> 
> c) suffers from a terminal illness that is likely to end the person’s life within 6 months (or a disease, illness, or medical condition that is neurodegenerative and likely to end his or her life within 12 months),
> 
> d) is in a state of irreversible decline in capability or experiences unbearable suffering that cannot be relieved in a manner that he or she considers tolerable,
> 
> e) has the ability to understand the nature of assisted dying and the consequences for him or her of assisted dying.
> 
> f) has not within the last 6 months made a request for assisted dying and subsequently changed their minds in favour of not completing the process of assisted dying.

**4. Request made for assisted dying**

1) A person eligible for assisted dying who wishes to request assisted dying may inform their attending medical practitioner of their wish. 

2) If the attending medical practitioner is unwilling to comply with the requirements of this Act they must request a replacement medical practitioner from the SCENZ group. 

3) If the attending medical practitioner is willing to comply with the requirements of this Act they must:

> a) Inform the person of the prognosis for their illness or condition, the irreversible nature of assisted dying, and the anticipated impacts of their assisted dying.
> 
> b) Talk to the person about their wish for assisted dying at intervals relative to the progress of their illness or condition.
> 
> c) Ensure that the person understands their alternative options for end of life care, and that they can change their mind and at any time.
> 
> d) Ensure that the person has the opportunity to talk about their wish with whomever they choose to, and that in expressing their wish they are free from pressure from any other person.
> 
> e) Have the person complete the prescribed form, giving their written consent, confirming that they are of sound mind, and listing any stipulations they personally want followed. This must be signed in the presence of two witnesses and the attending practitioner, all of whom must also sign the form. If the person is incapable of signing the form, one of the witnesses may sign for them with their consent.
> 
> f) Confirm that the above conditions have been satisfied before confirming the person’s request for assisted dying.

**4A. Assisted dying must not be initiated by registered health practitioner**

1) A registered health practitioner who provides health services or professional care services to a person must not, in the course of providing those services to the person in substance, suggest voluntary assisted dying to that person.

2) Nothing in subsection (1) prevents a registered health practitioner providing information about voluntary assisted dying to a person at that person’s request.

3) A contravention of subsection (1) is to be regarded as a breach of professional conduct.

**5. First opinion reached**

1) Once a request for assisted dying has been confirmed by the attending medical practitioner, a first opinion must be reached. 

2) The attending medical practitioner must reach the opinion that either:

> a) the person is a person who is eligible for assisted dying; or
> 
> b) the person is not a person who is eligible for assisted dying; or
> 
> c) the person would be eligible for assisted dying if the person’s competence were established as detailed in section 7(3).

**6. Second opinion reached**

1) Once the attending medical practitioner has reached their opinion they must request the name and details of an independent medical practitioner from the SCENZ group.

2) The independent medical practitioner must:

> a) read the person’s files; and
> 
> b) examine the person; and
> 
> c) reach an opinion as described in section 5(2)(a), 5(2)(b), or 5(2)(c).

**7. Third opinion reached, if necessary**

1) This section applies only if either the attending medical practitioner or the independent medical practitioner reached the opinion as described in section 5(2)(c). 

2) The medical practitioners must both request the name and details of a relevant independent specialist from the SCENZ group. 

3) The independent specialist must:

> a) read the person’s files; and
> 
> b) examine the person; and
> 
> c) reach the opinion that the person is either competent or is not competent.

**8. Negative decision made on request**

1) The following section applies if either the attending medical practitioner or the independent medical practitioner reached the opinion described in section 5(2)(b), or if the independent specialist reached the opinion that the person requesting assisted dying is not competent.

2) The medical practitioners or the independent specialist must explain the reasons to the person as to why their request for assisted dying was denied.

**9. Positive decision made on request**

1) The following section applies if both the attending medical practitioner and the independent medical practitioner reached the opinion described in section 5(2)(a), or if the medical practitioners reached the opinion described in section 5(2)(c) and subsequently the independent specialist reached the opinion that the person is competent.

2) The attending medical practitioner must inform the person that they are eligible for assisted dying and discuss with the person the progress of their terminal illness or irremediable medical condition.

3) The attending medical practitioner must discuss with the person the likely timing of their assisted dying and must confirm with the person which of the following four methods they would choose to administer the medicine for assisted dying: 

> a) ingestion triggered by the person; 
> 
> b) intravenous delivery, triggered by the person;
> 
> c) ingestion through a tube, triggered by the attending medical practitioner; or 
> 
> d) injection, triggered by the attending medical practitioner.

4) The attending medical practitioner must then have the person complete the prescribed form confirming the details of their request and consent for assisted dying, or must have someone complete the form in the presence of and with the consent of the person. This for must be signed in the presence of two witnesses and the attending medical practitioner, all of whom must sign the form also.

5) The attending medical practitioner must then make provisional arrangements for the medicine and themselves to be available at the time indicated for assisted dying.

**10. Medicine administered**

1) The following section applies if section 9 has been complied with.

2) At the chosen time of administration the attending medical practitioner must confirm that the person wishes to receive the medicine.

3) If the person confirms that they wish to receive the medicine, the attending medical practitioner must administer the medicine by the method chosen by the person, described in section 9(3).

4) The attending medical practitioner must be readily available for the person until they die, or must ensure that another medical practitioner is available.

5) The attending medical practitioner must then report the death and details of the death to the SCENZ group registrar and the independent review committee.

**11. SCENZ Group**

1) The Director-General of Health must establish the SCENZ Group by appointing to it the >number of medical practitioners, health practitioners, specialists, Māori and Pasifika >representatives, disabled persons organisations, anaesthetists, and pharmacists, and other persons that the Director-General considers appropriate.

2) The functions of the SCENZ Group are-

> a) to make and maintain a list of medical practitioners who are willing to act for the purposes of this Act as-
> 
> > i) replacement medical practitioners:
> > 
> > ii) independent medical practitioners:
> 
> b) to provide a name and contact details from the list maintained under paragraph (a), when this Act requires the use of a replacement medical practitioner or independent medical practitioner, in such a way as to ensure that the attending medical practitioner does not choose the replacement medical practitioner or independent medical practitioner:
> 
> c) to make and maintain a list of health practitioners who are willing to act for the purposes of this Act as specialists:
> 
> d) to provide a name and contact details from the list maintained under paragraph (c), when this Act requires the use of a specialist, in such a way as to ensure that neither the attending medical practitioner nor the independent medical practitioner chooses the specialist:
> 
> e) to make and maintain a list of pharmacists who are willing to dispense medication for the purposes of section 16:
> 
> f) to provide a name and contact details from the list maintained under paragraph (e) when section 16 is to be applied:
>
> g) in relation to the administration of medication under section 16,-
>
> h) to prepare standards of care; and
>
> > i) to advise on the required medical and legal procedures; and
> > 
> > ii) to provide practical assistance, if assistance is requested.

3) The Director-General must appoint an employee from the Ministry of Health to act as the registrar for the SCENZ group, who will compile a list of all deaths which occur from assisted dying in addition from the reports submitted by the attending medical practitioner in section 10(5).

4) The Director-General must establish an independent review committee consisting of a medical ethicist, a medical practitioner who practises in the area of end of life care, and another medical practitioner.

5) The independent review committee must consider all reports submitted by attending medical practitioners under section 10(5), must report to the SCENZ group registrar their satisfaction or dissatisfaction with the cases reported, and must recommended actions for the registrar to take if they are dissatisfied with any cases reported.

**12. Counselling for family**

1) After the medicine has been administered as prescribed under Section 11, family, close friends, and whanau are entitled to counselling if they are struggling to come to terms with the assisted death of the patient.

2) The Minister of Health must pay for the counselling, out of public money appropriated by Parliament for the purpose, to the counsellors for their service.

3) The counsellor may judge whether the family member, close friend, or whanau member does need the counselling or not, and may choose not to provide counselling at their discretion.

4) The counsellor may end the counselling programme at any point, judging that the counselling is no longer necessary.

**12A. Insurance**

The sale, procurement, or issuance of any life, health, or accident insurance or annuity policy or the rate charged for any policy shall not be conditioned upon or affected by the making or rescinding of a request, by a person, for medication to end his or her life in a humane and dignified manner under this Act. Neither shall a qualified patient’s act of ingesting medication to end his or her life in a humane and dignified manner have an effect upon a life, health, or accident insurance or annuity policy.

**13. Offences**

1) A person who commits an offense by:

> a) wilfully failing to comply with a requirement in this Act; or
> 
> b) partially completing a prescribed form for a person without the person’s consent; or
> 
> c) altering or destroying a completed or partially completed prescribed form without the consent of the person who completed or partially completed it.

—is liable for conviction to either:

> a) a term of imprisonment not exceeding 6 months; or
> 
> b) a fine not exceeding $10,000.

2) A person who commits an offense by:

> a) fully completing a prescribed form for a person without the person’s consent

—is liable for conviction to either:

> a) a term of imprisonment not exceeding 18 months; or
> 
> b) a fine not exceeding $30,000.

3) A person who commits an offense by fully completing a prescribed form for a person without the person’s consent, with the intent of causing harm or death upon the person, is liable to be charged with attempted murder. If a patient dies as a result of this offense, the person is liable to be charged with murder.

**14. Restrictions on advertising**

1) No person, unless authorised by this section, may publish in New Zealand, or arrange >for any other person to publish in New Zealand, an assisted dying advertisement.

2) A notice or sign must be treated as an assisted dying advertisement if the notice or >sign-

> a) communicates information that includes assisted dying health information or warnings, >assisted dying eligibility information or warnings, or both; and
> 
> b) is displayed inside of or immediately outside the premises of a health practitioner; and
> 
> c) is not required or permitted by this Act, regulations under this Act, or both.

3) In this section, assisted dying advertisement-

> a) means any words, whether written, printed, or spoken, including on film, video >recording, or other medium, broadcast or telecast, and any pictorial representation, design, >or device used to encourage the use or to notify the availability of assisted dying, and  >includes-
> 
> > i) any trade circular, any label, and any advertisement in any trade journal; and
> > 
> > ii) any depiction, in a film, video recording, telecast, or other visual medium, of assisted >dying, where in return for that depiction a sum of money is paid or any valuable thing is >given, whether to the maker or producer of that film, video recording, telecast, or visual >medium, or to any other person; and
> 
> b) does not include-
> 
> > i) the editorial content of-
> > 
> > > a) a periodical:
> > > 
> > > b) a radio or television programme:
> > > 
> > > c) a publication on a news media Internet site:
> > 
> > ii) any publication on the Internet, or other electronic medium, of personal views by an >individual who does not make or receive a payment in respect of the publication.

4) A retailer of medication used for the purpose of assisted dying may do all or
>any of the following things:

> a) provide, inside that retailer’s place of business, and on a request from a health >practitioner (however expressed), any information (in any medium, but only in the form of >printed, written, or spoken words) that-
> 
> > i) does no more than identify the medications that are available for purchase in that place >and indicate their price; and
> > 
> > ii) complies with any relevant regulations:
> 
> b) display the retailer’s name or trade name at the outside of the retailer’s place of business so long as the name is not and does not include any reference to assisted dying.

5) A health practitioner may give a person such information as is necessary to-

> a) determine that person’s eligibility for assisted dying in accordance with section 3:
> 
> b) provide that person with the information required by this Act
> 
> c) obtain the information required by this Act

6) A health practitioner may communicate with other health practitioners for the
purposes of this Act

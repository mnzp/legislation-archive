---
principal: C0002A00011 
upto: C0003A00013
---

#Criminal Justice Reform Act 2018

**1. Purpose**

The purpose of this Act is to modernise aspects of the Criminal Justice system and to put focus on rehabilitation rather than punishment. 

**2. Commencement**

1) This Act comes into force the day after it receives royal assent.

**3. Extension of the Youth Court’s jurisdiction**

*[Repealed]*

**4. Repeal of the Bail Amendment Act 2013**

1) The Bail Amendment Act 2013 is repealed.

**5. Repeal of the Electoral (Disqualification of Sentenced Prisoners) Amendment Act 2010**

1) The Electoral (Disqualification of Sentenced Prisoners) Amendment Act 2010 is repealed.
